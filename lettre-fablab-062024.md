# Revue de presse "Fablab" du mois de juin 2024 - La dernière !  

Mes activités ont évolué depuis la parution de la première lettre en janvier 2018 et j'ai depuis plus d'un an un collègue
brillant et enthousiaste qui s'occupe de l'électronique Open Source (Arduino, Raspberry, capteurs...) et de l'impression 3D dans notre équipe.

Il est donc temps pour moi de cesser la diffusion de cette revue de presse mensuelle, même si je continue à avoir un pied dans ce domaine passionnant puisque je collabore étroitement avec mon collègue, mais plutôt sur la partie administration systèmes.

Toujours passionné par l'open source, la sécurité informatique et la blockchain (https://forgemia.inra.fr/cedric.goby/blockchain-esr), j'ai désormais des activités autour de l’intelligence artificielle. D'ailleurs je lance une revue de presse sur cette thématique si ça vous intéresse : https://groupes.renater.fr/sympa/info/ia

Je remercie donc les plus de 400 abonné.e.s à la liste fablab@groupes.renater.fr, j'espère que vous y avez trouvé des choses intéressantes.  

## IMPRESSION 3D  

### 10 raisons pour lesquelles la numérisation 3D est utile en impression 3D
La numérisation 3D est un processus qui consiste à capturer la forme, la composition et les dimensions d’objets ou d’environnements à distance à l’aide de scanners 3D. Les données collectées par les lasers et les caméras peuvent être utilisées pour créer des modèles 3D de haute précision pour diverses applications. Avant d’aborder les applications spécifiques pour lesquelles la numérisation 3D peut être utile, examinons brièvement le fonctionnement du processus de numérisation.  
https://www.3dnatives.com/10-raisons-numerisation-3d-utile-limpression-3d-19062024/  
Source : 3Dnatives

### Une nouvelle méthode d’impression 3D FDM pour repousser les limites du multi-couleurs et multi-matériaux
Le dépôt de fil fondu (FDM) offre de nombreux avantages, notamment son accessibilité et sa facilité d’utilisation, ce qui rend la technologie attrayante. Cependant, la tête d’impression ne peut imprimer qu’un seul matériau, sous la forme de filament, à la fois, limitant les possibilirtés d’impression 3D multi-matériaux. Pour surmonter cette limitation, les chercheurs Sang-Joon Ahn, Howon Lee et Kyu-Jin Cho de l’Université Nationale de Séoul ont développé un processus unique en deux étapes pour l’impression 3D FDM, permettant de combiner différentes propriétés dans un seul matériau de base. En utilisant cette méthode Blended-FDM (b-FDM), ils ont réussi à imprimer un seul filament FDM avec 36 couleurs différentes en utilisant quatre filaments de couleur primaire.  
https://www.3dnatives.com/b-fdm-impression-3d-multi-couleurs-materiaux/  
Source : 3Dnatives

### Betteraves imprimées en 3D : un nouvel outil pour le phénotypage des plantes ?
La technologie dans l’industrie agricole progresse continuellement. Ces dernières années, l’adoption d’outils tels que les drones, les robots agricoles, les scanners, les planteurs de précision et l’intelligence artificielle a considérablement augmenté. Ces innovations ont principalement amélioré les rendements des cultures et facilité les pratiques de gestion intelligentes. En outre, l’impression 3D a apporté des avantages substantiels, en particulier dans la fabrication rapide d’outils agricoles. Les applications potentielles de cette technologie sont nombreuses, comme le montre une étude récente dans laquelle un modèle de betterave imprimé en 3D a été mis au point pour améliorer le phénotypage des plantes.  
https://www.3dnatives.com/betteraves-imprimees-en-3d-outil-phenotypage-27062024/  
Source : 3Dnatives

### Une nouvelle caméra 3D multispectrale qui tient dans la paume de la main développée grâce à la technologie Inkjet
Un groupe de chercheurs de l’Institut de Technologie de Karlsruhe, en Allemagne, a réussi à fabriquer une caméra 3D multispectrale compacte. Pour ce faire, ils ont imprimé un ensemble de micro-lentilles à l’aide de la technologie Inkjet (impression par jet d’encre). L’objectif principal de cette collaboration était de développer une nouvelle méthode pour la collecte de données optiques utiles à la reconstruction et à l’analyse précise des objets. Une technologie qui pourrait révolutionner les processus les plus avancés de numérisation 3D. Les résultats ont été satisfaisants, car cette nouvelle caméra peut obtenir des informations et des données 3D en une seule prise.  
https://www.3dnatives.com/camera-3d-multispectrale-inkjet/  
Source : 3Dnatives

### Une première imprimante 3D sur puce qui tient dans la paume de la main
Alors que la tendance est à la miniaturisation, le MIT et l’université du Texas à Austin viennent de révéler leur premier prototype d’une imprimante 3D “on chip” c’est-à-dire sur puce, qui pourrait tenir dans la paume de la main. Il s’agit d’une petite machine résine qui s’appuie sur le principe de photopolymérisation : grâce à de minuscules antennes optiques, la puce est capable d’orienter un faisceau lumineux qui peut alors durcir la résine liquide. Les chercheurs expliquent avoir combiné la photonique au silicium et la photochimie pour développer ce procédé d’impression 3D et concevoir en quelques secondes seulement les lettres M-I-T comme preuve de concept. A l’avenir, ils espèrent que leur machine pourra permettre la fabrication de nombreuses applications, notamment dans le secteur médical.  
https://www.3dnatives.com/imprimante-3d-sur-puce-mit-110620243/  
Source : 3Dnatives

### Du grain de café aux meubles imprimés en 3D
Ces dernières années, les efforts pour promouvoir une économie circulaire dans le monde entier se sont multipliés, et l’un des domaines qui va de pair avec ce modèle est la conception circulaire. La nécessité de créer des produits respectueux de l’environnement est incontestable. À cette occasion, nous avons souhaité vous présenter un projet qui cherche à promouvoir la durabilité en redéfinissant le design d’intérieur. L’entreprise espagnole Lowpoly a collaboré avec le groupe de café D-Origen pour créer des meubles imprimés en 3D pour un nouveau café à Barcelone. Les meubles sont fabriqués à partir de déchets de café et sont imprimés en 3D.  
https://www.3dnatives.com/lowpoly-cafe-impression-3d-13062024/  
Source : 3Dnatives

### Le mycélium comme matériau d’impression 3D
Le plastique étant l’un des matériaux les plus répandus sur terre, son impact sur le climat a fait l’objet de nombreuses discussions. Cependant, quiconque croit que le plastique est le seul coupable parmi les matériaux nuisibles à l’environnement se trompe. L’industrie du ciment se classe au troisième rang en termes d’émissions de CO2 (selon le Programme des Nations unies pour l’environnement 2020) et il suffit de regarder autour de soi pour constater à quel point il est utilisé. Heureusement, des recherches intensives sont menées sur de nouveaux matériaux qui peuvent être utilisés comme alternatives. L’un de ces matériaux est le mycélium ou mycélium/mycelium, à base de champignons. Associé à l’impression 3D, ce matériau biosourcé présente un grand potentiel pour une industrie de la construction respectueuse de l’environnement et de nombreuses applications.  
https://www.3dnatives.com/mycelium-materiau-impression-3d-17062024/  
Source : 3Dnatives

### Les projets biomimétiques qui utilisent l’impression 3D
Le biomimétisme, tel que défini par Cambridge, est « la pratique consistant à imiter les processus naturels dans la conception technologique et industrielle ». Bien qu’il puisse sembler paradoxal de ramener les avancées technologiques au monde naturel, cette approche est en réalité motivée par la reconnaissance croissante parmi les ingénieurs et les scientifiques que, souvent, la nature offre les meilleures solutions en matière de conception. De plus, avec le développement des technologies d’impression 3D, ces conceptions complexes deviennent réalisables, contrairement aux limitations des technologies soustractives. C’est pourquoi nous avons décidé d’explorer, dans la liste suivante, certains des projets qui combinent impression 3D et biomimétisme, afin de mieux comprendre comment la fabrication additive nous permet de concrétiser ces modèles optimisés.  
https://www.3dnatives.com/projets-biomimetisme-impression-3d-20062024/  
Source : 3Dnatives

### Comment stocker correctement vos filaments d’impression 3D ?
L’impression 3D FDM/FFF est de plus en plus populaire, surtout pour un usage privé. Que ce soit à la maison ou au bureau, l’impression 3D avec des filaments n’est pas seulement un moyen pratique de créer des objets de tous les jours, elle donne également libre cours à la créativité. Mais cela ne veut pas dire qu’il n’y a pas de défis à relever. Par exemple, stocker de façon optimale son filament en fait partie.  
https://www.3dnatives.com/stocker-filament-impression-3d-06062024/  
Source : 3Dnatives

### La 10ème édition du 3D PRINT Lyon 2024 a-t-elle tenu ses promesses ?
Après le succès de l’édition 2022, le 3D PRINT Congress & Exhibition 2024 a rouvert ses portes à Lyon du 4 au 6 juin dernier, clôturant une édition particulièrement marquante. En effet, cette année, le salon célébrait son 10ème anniversaire, un moment important pour cet événement de l’impression 3D. Près de soixante nouveaux exposants français et internationaux ont participé pour la première fois, rejoignant un total de 300 exposants. Avec 80 conférences et ateliers, l’événement a attiré plus de 6 000 participants qui sont venus découvrir les dernières innovations de l’impression 3D. Au programme : remises de prix, conférences et nouveaux exposants. Alors que retenir de cette édition 2024 ? Quels étaient les incontournables ? Revenons sur les principaux temps forts du salon.  
https://www.3dnatives.com/bilan-salon-3d-print-lyon-2024-100620243/  
Source : 3Dnatives

## ORDINATEURS MONO-CARTES  

### AlmaLinux prend désormais en charge le Raspberry Pi 5
« La sortie du Raspberry Pi 5 en octobre 2023, avec ses améliorations et mises à jour significatives, a suscité encore plus d’intérêt pour AlmaLinux sur Raspberry Pi », explique Koichiro Iwao, ingénieur chez Cybertrust et développeur AlmaLinux.  
https://goodtech.info/almalinux-prend-desormais-en-charge-le-raspberry-pi-5/  
Source : Goodtech Info

### Parrot OS 6.1 (Debian) renforce votre sécurité et se colle au Raspberry Pi 5
La distribution Linux sécuritaire Parrot 6.1, déclinaison spécialisée de Debian, comprend un éventail de nouvelles fonctionnalités, des mises à jour de sécurité et de stabilité. La version Raspberry Pi 5 se bonifie pour l’occasion.  
https://cercll.wordpress.com/2024/06/07/parrot-os-6-1-debian-renforce-votre-securite-et-se-colle-au-raspberry-pi-5/  
Source : CERCLL

### OpenCat – Le framework open source des animaux de compagnie robotiques
Aujourd’hui, j’aimerai vous parler OpenCat, un framework open source qui va vous aider à créer vos propres robots animaux de compagnie, c’est à dire des quadrupèdes hyper réalistes et Ô surprise parfaitement abordables. Pour cela, OpenCat permet de piloter des servomoteurs haute performance utilisés comme articulations, une structure de corps optimisée et des contrôleurs low-cost comme Arduino, ESP32 ou Raspberry Pi.  
https://korben.info/opencat-framework-open-source-robots-animaux-compagnie.html  
Source : Korben

## IA  

### Sortie d’un Kit Raspberry Pi AI à base de M2 HAT et de l’accélérateur AI Hailo 8L
En mai je vous ai présenté le kit M2 HAT pour le Raspberry Pi 5. Il permet d’accueillir des SSD 2230 et 2242. La Fondation a prévu un kit qui va faire son effet : La carte M2 HAT est en effet capable d’accueillir un accélérateur IA (intelligence artificielle) Hailo 8L qui peut monter jusque 13 TOPS (Trillons d’opération par seconde). De quoi booster bien des projets, non ?  
https://www.framboise314.fr/sortie-dun-kit-raspberry-pi-ai-a-base-de-m2-hat-et-de-laccelerateur-ai-hailo-8l/  
Source : Framboise 314

### Intelligence artificielle : 5 raisons de préférer Perplexity aux autres chatbots d'IA
La qualité des sources n'est qu'un des atouts qui place Perplexity au-dessus de ChatGPT et Copilot. Voici pourquoi.  
https://www.zdnet.fr/guide-achat/intelligence-artificielle-5-raisons-de-preferer-perplexity-a-tous-les-autres-chatbots-dia-392346.htm  
Source : ZDNet

### VoiceCraft – Enfin de la synthèse vocale de qualité
VoiceCraft c’est tout simplement le futur de l’édition vocale et de la synthèse vocale. On est carrément dans de la science-fiction là puisque juste avec juste quelques secondes d’audio de votre voix, cette IA est capable de comprendre votre timbre, votre intonation… votre flow quoi. Et après, c’est parti mon kiki, vous pouvez lui faire dire ce que vous voulez, et ça sonnera exactement comme si c’était vous qui parliez ! Flippant et génial à la fois.  
https://korben.info/voicecraft-ia-revolutionne-edition-vocale-synthese.html  
Source : Korben

## FABLABS  

### Les inscriptions à Faire Tiers-Lieux sont ouvertes !
Du 8 au 10 octobre 2024 à Toulouse, les Halles de la Cartoucherie deviendront le théâtre d’une nouvelle aventure collective : notre deuxième événement Faire Tiers-Lieux, placé sous le signe du “faire ensemble” et de la coopération.  
https://francetierslieux.fr/les-inscriptions-a-faire-tiers-lieux-sont-ouvertes/  
Source : France Tiers Lieux

## TUTOS  

### Jéjé l’ingé nous propose de construire un PipBoy de bureau
Jéjé l’ingé propose des vidéos de ses créations où il explique pas à pas comment fabriquer des objets du quotidien avec des designs originaux tirés le plus souvent du folklore cinématographique.  
https://www.minimachines.net/actu/jeje-linge-nous-propose-de-construire-un-pipboy-de-bureau-127670  
Source : Minimachines.net

## AGENDA  

### SIDO et Lyon Cyber Expo – Les 18 et 19 septembre 2024 – Lyon
Le SIDO, le rendez-vous incontournable de l’IoT, de l’IA, de la XR et de la robotique fête ses 10 ans ! Et s’enrichit pour l’occasion d’une Cyber Expo…  
https://www.itforbusiness.fr/sido-et-lyon-cyber-expo-les-18-et-19-septembre-2024-lyon-78106  
Source : IT for Business

---  
Cédric Goby / UMR AGAP / INRAE (Institut national de la recherche pour l’agriculture, l’alimentation et l’environnement)  
**Cette lettre est publiée sous la licence Attribution 4.0 International (CC BY 4.0)**  

---  
Découvrez les autres revues de presse !  
Revue de presse "Sécurité informatique" : https://groupes.renater.fr/sympa/info/securite-informatique  
Revue de presse "Open source" : https://groupes.renater.fr/sympa/info/open-source  



